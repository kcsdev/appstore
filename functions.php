<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

include( 'core/bootstrap.php' );


// Custom Functions by Tomer

// Shortcodes
    //[first_app]
function first_app_func( $atts ){
	include_once("shortcodes/first_app.php");
    return;
}
add_shortcode( 'first_app', 'first_app_func' );

    //[developer_area]
function developer_area_func( $atts ){
	include_once("shortcodes/developer_area.php");
    return;
}
add_shortcode( 'developer_area', 'developer_area_func' );

    //[comp_icons]
function comp_icons_func( $atts ){
	include_once("shortcodes/comp_icons.php");
    return;
}
add_shortcode( 'comp_icons', 'comp_icons_func' );

    //[app_section_1]
function app_section_1_func( $atts ){
	include_once("shortcodes/app_section_1.php");
    return;
}
add_shortcode( 'app_section_1', 'app_section_1_func' );

    //[app_section_2]
function app_section_2_func( $atts ){
	include_once("shortcodes/app_section_2.php");
    return;
}
add_shortcode( 'app_section_2', 'app_section_2_func' );

// EOF