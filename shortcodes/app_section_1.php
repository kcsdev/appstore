<div class='app_section_1'>
        <div class='single_app'>
            <div class='profile_image_cont'>
                <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/profile_image.jpg'>
            </div>
            <div class='profile_image_icon'>
                <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/profile_icon.png'>
            </div>
                <p class='app_title'>ניהול החשבון בטאבלט</p>
                <p class='app_author'>פועלים דיגיטל</p>
            <div class='app_popup'>
                <a href='#'>לפרטים והורדה</a>
            </div>
                <p class='app_desc'>נהל את החשבון האישי שלך מרחוק.
        בדוק מצב חשבון, העבר כספים, נהל תיקי
        השקעות.</p>
            <div class='app_bar'>
                <div class='app_comp'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/tablet.png'>
                        <span>טאבלט</span>
                    </a>
                </div>
                <div class='app_reviews'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/apple.png'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/4stars.png'>
                    </a>
                </div>
            </div>
        </div>
        <div class='single_app'>
            <div class='profile_image_cont'>
                <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/profile_image.jpg'>
            </div>
            <div class='profile_image_icon'>
                <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/profile_icon.png'>
            </div>
                <p class='app_title'>הארנק הדיגיטלי</p>
                <p class='app_author'>פועלים דיגיטל</p>
            <div class='app_popup'>
                <a href='#'>לפרטים והורדה</a>
            </div>
                <p class='app_desc'>נהל את החשבון האישי שלך מרחוק.
        בדוק מצב חשבון, העבר כספים, נהל תיקי
        השקעות.</p>
            <div class='app_bar'>
                <div class='app_comp'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/smartphone.png'>
                        <span>סמארטפון</span>
                    </a>
                </div>
                <div class='app_reviews'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/apple.png'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/4stars.png'>
                    </a>
                </div>
            </div>
        </div>
        <div class='single_app new_idea'>

                <p class='app_title'>האפליקציה שלך
יכולה להיות כאן</p>
              
            
                <p class='app_desc'>יש לך רעיון?
צורך באפליקציה שתשפר את החיים?
אנו מזמינים אותך לשלוח את הצעתך
ואולי נפתח גם את האפליקציה שלך.</p>
            <div class='app_popup'>
                <a href='#'>להגשת הצעה</a>
            </div>
           
        </div>
        <div class='single_app'>
            <div class='profile_image_cont'>
                <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/profile_image.jpg'>
            </div>
            <div class='profile_image_icon'>
                <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/profile_icon.png'>
            </div>
                <p class='app_title'>Smart Watch</p>
                <p class='app_author'>פועלים דיגיטל</p>
            <div class='app_popup'>
                <a href='#'>לפרטים והורדה</a>
            </div>
                <p class='app_desc'>נהל את החשבון האישי שלך מרחוק.
        בדוק מצב חשבון, העבר כספים, נהל תיקי
        השקעות.</p>
            <div class='app_bar'>
                <div class='app_comp'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/clock.png'>
                        <span>שעון חכם</span>
                    </a>
                </div>
                <div class='app_reviews'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/apple.png'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/4stars.png'>
                    </a>
                </div>
            </div>
        </div>
</div>
