<div class='developer_area'>
    <div class='top_area'>
        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/developer.png'>
    </div>
    <div class='bottom_area'>
        
            <p class='dev_title'>מהפיכת ה-Open Banking כאן!</p>
            <p class='dev_desc'>לראשונה, פועלים חושפים API ומאפשרים לכם לפתח
אפליקציות ולהתחבר ישירות עם לקוחות הבנק. בואו
ליצור את הדבר הבא ולהיחשף בחנות האפליקציות.</p>
        <div class='dev_popup'>
            <a href='#'>לפרטים והורדה</a>
        </div>
    </div>
</div>