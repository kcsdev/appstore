<div class='comp_icons'>
            <div class='app_bar'>
                <div class='app_comp'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/dark/tablet.png'>
                        <span>טאבלט</span>
                    </a>
                </div>
                <div class='app_comp'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/dark/smartphone.png'>
                        <span>סמארטפון</span>
                    </a>
                </div>
                <div class='app_comp'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/dark/clock.png'>
                        <span>שעון חכם</span>
                    </a>
                </div>
                <div class='app_comp'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/dark/apple.png'>
                        <span>Apple</span>
                    </a>
                </div>
                <div class='app_comp'>
                    <a href='#'>
                        <img src='<?php echo get_template_directory_uri(); ?>/shortcodes/temp_images/dark/android.png'>
                        <span>Android</span>
                    </a>
                </div>
            </div>
        </div>
</div>
